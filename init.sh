#!/bin/bash


update_file() {
	set +x # remove option temporarily
	diff $1 $2 >/dev/null 2>&1
	ret=$?
	if [ $ret -ne 0 ]; then
		echo "Updating $2"
		cp $1 $2
		set -x
		return 0
	else
		echo "Skip $2"
		set -x
		return 1
	fi
}


if [ "$EUID" -ne 0 ]; then # check sudo
	echo "Please run as root"
	exit 1
fi



set -eux

cd $(dirname $0)

systemctl stop isuda.perl.service
systemctl stop isutar.perl.service

systemctl disable isuda.perl.service
systemctl disable isutar.perl.service


(
    cd webapp/go
    make
)

rsync -av webapp/go/ ~/webapp/go/
rsync -av etc/nginx/ /etc/nginx/
rsync -av etc/mysql/ /etc/mysql/
rsync -av etc/sysctl.conf /etc/
rsync -av etc/security/ /etc/security/

rm -rf /var/log/nginx/main_access.log
service nginx restart

systemctl restart isuda.go.service
systemctl restart isutar.go.service
systemctl restart isupam.service

systemctl enable isuda.go.service
systemctl enable isutar.go.service
