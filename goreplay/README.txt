# Quick Start!
$ ./install.sh
$ ./capture.sh  # パケットキャプチャする。適当なところでCtrl + C.
$ ./replay.sh -m '--verbose' # キャプチャしたものを再生する。（冗長モードで）

# goreplayについて
https://goreplay.org/
パケットキャプチャをしてHTTP requestを取り出し、取り出したrequestを再生することができるツール


## install.sh
goreplayをインストールする。
また、middle.rbを動かすために必要なgem install httpも実行する。


## 80番ポートのトラフィックをキャプチャする
$ sudo goreplay --input-raw :80 --output-file=requests.gz --input-raw-track-response 
もしくは
$ ./capture.sh

--input-raw-track-response: HTTPリクエストだけでなく、サーバーからのレスポンスも保存しておく
好きなところでCtrl+Cで中断する。
requests_0.gzにリクエストとレスポンスが保存される。


## 保存したものをリプレイする
$ time sudo goreplay --input-file ./requests_0.gz --middleware ./middle.rb --output-http=http://localhost --output-http-track-response
もしくは
$ ./replay.sh

オプション"--middleware ./middle.rb"を与えると統計情報や元のレスポンスとの違いを測定してくれる。


## 2倍の速さでリプレイ
$ time sudo goreplay --input-file "requests_0.gz|200%" --middleware ./middle.rb --output-http=http://localhost --output-http-track-response
もしくは
$ ./replay.sh 200


## 統計情報について
takelog.shの統計情報とかなり異なっている部分がある。
これは302 Foundのときに起こっている気がする。
つまりmiddle.rbの統計情報はRedirect先の計算時間は含んでいないはず。


## 距離の測り方
各行ごとにハミング距離を計算して、行全体で平均をとったものを距離としている。
