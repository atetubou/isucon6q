#!/bin/bash

set -eux

install_ruby() {
	echo "install http for middle.rb"
	sudo apt -y install ruby ruby-dev
	sudo gem install http
	sudo gem install rake
	sudo gem install rainbow
}

install_ruby

install_goreplay() {
	if which goreplay
	then
		echo "goreplay is already installed"
		return 1
	fi

	echo "Install goreplay"

	wget https://github.com/buger/goreplay/releases/download/v0.16.1/gor_0.16.1_x64.tar.gz
	tar zxvf gor_0.16.1_x64.tar.gz 
	sudo mv goreplay /usr/local/bin/
	rm gor_0.16.1_x64.tar.gz 

	echo "ok"
}

install_goreplay

